@startuml pi-bridge
title piをスマホ配下のhot spotとして使用

cloud wan
agent コンセント
frame box {
    agent smartPhone [
        smartPhone
        ....
        usbテザリング有効
    ]
    smartPhone . wan
    smartPhone -- コンセント
    agent pi [
        pi
        ....
        hot spot(bridge)
    ]
    pi - smartPhone
}

agent PC
pi .. PC : wifi

@enduml
